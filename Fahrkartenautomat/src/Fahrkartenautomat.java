﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rückgabebetrag;
		int anzahlTicket;

		double[] ticketPreise = new double[10];

		ticketPreise[0] = 1.90;
		ticketPreise[1] = 2.90;
		ticketPreise[2] = 3.30;
		ticketPreise[3] = 3.60;
		ticketPreise[4] = 8.60;
		ticketPreise[5] = 9.00;
		ticketPreise[6] = 9.60;
		ticketPreise[7] = 23.50;
		ticketPreise[8] = 24.30;
		ticketPreise[9] = 24.90;

		String[] ticketArt = new String[10];

		ticketArt[0] = "Kurzstrecke";
		ticketArt[1] = "Einzelfahrschein Berlin AB";
		ticketArt[2] = "Einzelfahrschein Berlin BC";
		ticketArt[3] = "Einzelfahrschein Berlin ABC";
		ticketArt[4] = "Tageskarte Berlin AB";
		ticketArt[5] = "Tageskarte Berlin BC";
		ticketArt[6] = "Tageskarte Berlin ABC";
		ticketArt[7] = "Kleingruppen-Tageskarte Berlin AB";
		ticketArt[8] = "Kleingruppen-Tageskarte Berlin BC";
		ticketArt[9] = "Kleingruppen-Tageskarte Berlin ABC";

		do {
			zuZahlenderBetrag = farkartenBestellungErfassen(ticketPreise);

			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
			System.out.println("Neuer Kaufvorgang");
		} while (true);
	}

	public static double farkartenBestellungErfassen(double[] ticketPreise) {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("Wählen Sie ihre Wunschfahrkarte aus:\n" + "  Kurzstrecke [1,90 EUR] (0)\n" + " \n"
				+ "Wählen Sie ihre Wunschfahrkarte für Berlin Einzelfahrtausweis aus:\n"
				+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
				+ "  Einzelfahrschein Regeltarif BC [3,30 EUR] (2)\n"
				+ "  Einzelfahrschein Regeltarif ABC [3,60 EUR] (3)\n" + " \n"
				+ "Wählen Sie ihre Wunschfahrkarte für Berlin Tageskarte aus:\n"
				+ "  Tageskarte Regeltarif AB [8,60 EUR] (4)\n" + "  Tageskarte Regeltarif BC [9,00 EUR] (5)\n"
				+ "  Tageskarte Regeltarif ABC [9,60 EUR] (6)\n" + " \n"
				+ "Wählen Sie ihre Wunschfahrkarte für Berlin Gruppen-Tageskarte aus:\n"
				+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (7)\n"
				+ "  Kleingruppen-Tageskarte Regeltarif BC [24,30 EUR] (8)\n"
				+ "  Kleingruppen-Tageskarte Regeltarif ABC [24,90 EUR] (9)\n");

		int ticketNr = tastatur.nextInt();
		
		double preisfuerEinTicket = ticketPreise [ticketNr];

		System.out.print("Anzahl der Tickets: ");
		int anzahlTicket = tastatur.nextInt();

		if (anzahlTicket > 10 || anzahlTicket < 1) {
			anzahlTicket = 1;
			System.out.println("Nur Eingabe zwischen 1 und 10 gültig. Anzahl Ticket wurde auf 1 gesetzt");
		}

		double zuZahlenderBetrag = preisfuerEinTicket * anzahlTicket;
		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double ausstehendeBetrage = zuZahlenderBetrag;
			ausstehendeBetrage = ausstehendeBetrage - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f €\n", ausstehendeBetrage);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

	}

}